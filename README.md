# DevOps Test Task
  
## Table of contents

### 1. [ERVCP](#ervcp)  
### 1. [CI/CD Pipeline](#cicd-pipeline)
### 2. [GKE Cluster](#gke-cluster)
### 3. [Scaling](#scaling)

# ERVCP 
- Web-app exposed on LoadBalancer [here](http://35.195.67.149:8080/)
- Connected to redis running in Kubernetes cluster
- [Dockerfile](/app/Dockerfile)
  
# CI/CD Pipeline
- [GitLab Pipeline](.gitlab-ci.yml)
- Every push to master branch triggers pipeline
- Deployed through gitlab-runner
- 3 stages (test stage is only simulated)
  - Build Docker image and push it to my Docker Hub with pipeline ID as image tag
  - Execute unit tests (simulated - no tests are present, only for demonstration)
  - Deploy to Kubernetes cluster namespace "ervcp"
- Using custom Docker image for gitlab-runner
  - Image is based on docker:18.09.7 with added curl and kubectl for managing deployments to Kubernets cluster
  - Dockerfile for this custom image is below
  
```
FROM docker:18.09.7  
RUN apk update 
RUN apk add curl 
RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" 
RUN chmod u+x kubectl && mv kubectl /bin/kubectl
```
  
# GKE Cluster
- Created in europe-west1-b	location
- 3 nodes, 6vCPUs and 22.5GB RAM total
- Integrated with GitLab
  
# Scaling
- Autoscaling set up for ERVCP web-app
  - Threshold for autoscaling is 60% average CPU utilization across all pods
  - Minimum number of replicas 1, maximum 5
